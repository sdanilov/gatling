package LoadTest

import LoadTest.Scripts._
import io.gatling.core.Predef.scenario

object Scenarios {

  val Scn_OpenHomePage = scenario("UC01_OpenHomePage").exec(UC01_OpenHomePage.homePage)

  val Scn_AddNewComputer = scenario("UC04_AddNewComputer").exec(UC04_AddNewComputer.addNewComputer)

  val Scn_DeleteComputer  = scenario("UC05_DeleteComputer").exec(UC05_DeleteComputer.DeleteComputer)

}
