package LoadTest

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import TestPlan._
import Scripts._
import Scenarios._

import scala.concurrent.duration._

class Runner extends  Simulation {

  val httpProtocol = http
    .baseUrl("http://computer-database.gatling.io") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
    .userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0")


/*  setUp(Scn_OpenHomePage.inject(StabilityTesting)
  ).protocols(httpProtocol).maxDuration(5 minutes)*/

/*  setUp(Scn_OpenHomePage.inject(MaxPerfTesting)
  ).protocols(httpProtocol).maxDuration(5 minutes)*/

/*  setUp(Scn_AddNewComputer.inject(MaxPerfTesting)
  ).protocols(httpProtocol).maxDuration(5 minutes)*/

  setUp(Scn_AddNewComputer.inject(constantConcurrentUsers(1).during(30)
  )).protocols(httpProtocol).maxDuration(30 seconds)

}
