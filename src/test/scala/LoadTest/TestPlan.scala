package LoadTest

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

//definition of all test's plans

object TestPlan {


  val StabilityTesting = Seq(
    rampConcurrentUsers(0) to (5) during(10),
    constantConcurrentUsers(5) during(60)
  )


  val MaxPerfTesting = Seq(
    incrementUsersPerSec(2)
      .times(5)
      .eachLevelLasting(10)
      .separatedByRampsLasting(10)
      .startingFrom(2)
  )
}
