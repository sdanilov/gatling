package LoadTest.Scripts

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object UC02_Search {

  val headers_0 = Map("Upgrade-Insecure-Requests" -> "1")

  val headers_1 = Map("Accept" -> "text/css,*/*;q=0.1")

  val feeder = csv("search.csv").circular

  val search = feed(feeder)
    .exec(
      http("/computers")
        .get("/computers")
        .headers(headers_0)
        .resources(
          http("/assets/stylesheets/bootstrap.min.css")
            .get("/assets/stylesheets/bootstrap.min.css")
            .headers(headers_1),
          http("/assets/stylesheets/main.css")
            .get("/assets/stylesheets/main.css")
            .headers(headers_1)
        )
        .check(status.is(200)))
    .exec(
      http("/computers?f=")
        .get("/computers?f=${searchCriterion}")
        .check(css("a:contains('${searchComputerName}')", "href").saveAs("computerURL"))
    )
    .pause(7)
    .exec(
      http("Select")
        .get("${computerURL}")
        .check(status.is(200)))



}
