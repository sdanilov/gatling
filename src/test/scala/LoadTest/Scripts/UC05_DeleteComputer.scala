package LoadTest.Scripts

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object UC05_DeleteComputer {

  val headers_0 = Map("Upgrade-Insecure-Requests" -> "1")

  val headers_1 = Map("Accept" -> "text/css,*/*;q=0.1")

  val DeleteComputer =
    exec(
      http("</computers")
        .get("/computers")
        .headers(headers_0)
        .resources(
          http("/assets/stylesheets/bootstrap.min.css")
            .get("/assets/stylesheets/bootstrap.min.css")
            .headers(headers_1),
          http("/assets/stylesheets/main.css")
            .get("/assets/stylesheets/main.css")
            .headers(headers_1)
        )
        .check(status.is(200)))
      .pause(5)
    .exec(
      http("</computers?f=")
        .get("/computers")
        .queryParam("f", "Everest")
        .check(regex("a href=\"/computers/(.+?)\">").findRandom.saveAs("computerForDelete"))
      .check(status.is(200))
    )
    .pause(5)
    .exec(
      http("</computer/")
      .get("/computers/${computerForDelete}")
    )
    .pause(5)
    .exec(
      http(">delete")
        .post("/computers/${computerForDelete}/delete")
        .disableFollowRedirect
        .check(status.is(303))
    )
    .exec(
      http("/computers after delete")
        .get("/computers")
        .check(status.is(200))
    )

}
