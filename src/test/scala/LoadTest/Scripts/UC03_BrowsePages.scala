package LoadTest.Scripts

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object UC03_BrowsePages {

  val headers_0 = Map("Upgrade-Insecure-Requests" -> "1")

  val headers_1 = Map("Accept" -> "text/css,*/*;q=0.1")

  val browse =
    exec(
      http("/computers")
        .get("/computers")
        .headers(headers_0)
        .resources(
          http("/assets/stylesheets/bootstrap.min.css")
            .get("/assets/stylesheets/bootstrap.min.css")
            .headers(headers_1),
          http("/assets/stylesheets/main.css")
            .get("/assets/stylesheets/main.css")
            .headers(headers_1)
        )
        .check(status.is(200)))
  repeat(5, "n") {
    exec(
      http("Page" + "${n}")
        .get("/computers?p=" + "${n}")
        .check(status.is(200))
    )
  }
}
