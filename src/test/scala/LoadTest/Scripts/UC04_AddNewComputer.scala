package LoadTest.Scripts

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object UC04_AddNewComputer {

  private val dateFmt = "yyyy-MM-dd"

  val date = new Date
  val sdf = new SimpleDateFormat(dateFmt)
  val introduced =  sdf.format(date)

  val calendar = Calendar.getInstance()
  calendar.roll(Calendar.MONTH, 2)
  val discontinued = sdf.format(calendar.getTime())


  val headers_0 = Map("Upgrade-Insecure-Requests" -> "1")

  val headers_1 = Map("Accept" -> "text/css,*/*;q=0.1")

  val addNewComputer =
    exec(
      http("</computers")
        .get("/computers")
        .headers(headers_0)
        .resources(
          http("/assets/stylesheets/bootstrap.min.css")
            .get("/assets/stylesheets/bootstrap.min.css")
            .headers(headers_1),
          http("/assets/stylesheets/main.css")
            .get("/assets/stylesheets/main.css")
            .headers(headers_1)
        )
        .check(status.is(200)))
    .pause(5)
      .exec(
        http("</computers/new")
          .get("/computers/new")
          .check(status.is(200))
      )
    .pause(5)
    .exec(
      http(">/computers")
        .post("/computers")
        .formParamMap(Map("name" -> "Everest", "introduced" -> introduced, "discontinued" -> discontinued, "company" -> "1"))
        .disableFollowRedirect
        .check(status.is(303))
    )
}
